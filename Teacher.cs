using System;
using System.Collections.Generic;

namespace comp5002_9987794_assessment2
{
    public class Teacher : Person
    {
        public Teacher(int _id, string _userName, string _name):base(_id, _name, _userName)
        {}

        public string WhatAmITeaching(List<Subject> subjects, string name)
        {
            string teasub = $"{name}: \n";

            //Loop for teacher name and subject they are teaching
            foreach (var subj in subjects)
            {
                if(subj.Teacher == name)
                {
                    teasub += subj.CourseName + "\n";
                }
            }

            return teasub;   

        }
  
    }
}