﻿using System;
using System.Collections.Generic;

namespace comp5002_9987794_assessment2
{
    class Program
    {
        static void Main(string[] args)
        {
        
        //Start the program with Clear();
        Console.Clear();
        
        //Create 3 lists
        //1.Students
        //2.Teachers
        //3.Subjects
    
        List<Student> students = new List<Student>();
        List<Teacher> teachers = new List<Teacher>();
        List<Subject> subjects = new List<Subject>();

        //Student List
        students.Add(new Student(44675, "dmohekey", "Dani Mohekey"));
        students.Add(new Student(65974, "brjones", "Bob Jones"));
        students.Add(new Student(16820, "jtuskan", "James Tuskan"));
        students.Add(new Student(92475, "flittle", "Frank Little"));

        //Teacher List
        teachers.Add(new Teacher(00179, "lpeters", "Luis Peters"));
        teachers.Add(new Teacher(00158, "mrobinson", "Mike Robinson"));
        teachers.Add(new Teacher(00162, "tsmith", "Tony Smith"));
        teachers.Add(new Teacher(00134, "swilliams", "Sarah Williams"));
        teachers.Add(new Teacher(00188, "cchaplin", "Charlie Chaplin"));
        teachers.Add(new Teacher(00146, "estone", "Emma Stone"));

        
        //Subject List
        subjects.Add(new Subject{CourseName = "Intro To Programming", CourseCode = "ITO-2017", Teacher = "Tony Smith"});
        subjects.Add(new Subject{CourseName = "Software Packages", CourseCode = "SWP-2017", Teacher = "Luis Peters"});
        subjects.Add(new Subject{CourseName = "Multimedia Studies", CourseCode = "MMS-2017", Teacher = "Charlie Chaplin"});
        subjects.Add(new Subject{CourseName = "Professional Skills", CourseCode = "PROS-2017", Teacher = "Sarah Williams"});


        //First Loop displays what teachers are teaching, using for loops because I understand them.
        for (int a = 0 ; a < teachers.Count; a++)
        {  
            Console.WriteLine(teachers[a].WhatAmITeaching(subjects, teachers[a].Name));
        }


        //Second Loop displays what subjects students are taking
        for (int a = 0 ; a < students.Count; a++)
        {
           Console.WriteLine(students[a].ListAllMySubjects(subjects)); 
        }


        //CS File for every class
        //Only console writeline in main method

        
        //End the program with blank line and instructions
        Console.ResetColor();
        Console.WriteLine();
        Console.WriteLine("Press <Enter> to quit the program");
        Console.ReadKey();

        }
    }
}
