using System.Collections.Generic;

namespace comp5002_9987794_assessment2
{
    public class Student : Person
    {
        public Student(int _id, string _userName, string _name):base(_id, _name, _userName) 
        {}

        public string ListAllMySubjects(List<Subject> subjects)
        {
            string stusub = $"{Name}: \n";

            //Loop for student name and subject they are taking.
            foreach (var s in subjects)
            {
                stusub += s.CourseName + "\n";
            }
            return stusub;

        }

    }
}