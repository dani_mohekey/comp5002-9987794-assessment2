    public class Person
    {

        public int ID {get; set; }
        public string Name { get; set; }
        public string Username { get; set; }

        public Person( int _ID, string _Name, string _Username)
        {
            ID = _ID;
            Name = _Name;
            Username = _Username;
        }


    }